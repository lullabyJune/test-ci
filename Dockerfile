FROM node:10

ARG GIT_ADDRESS
ARG BRANCH
ARG PROJECT_NAME
# ADD . /test/
# WORKDIR /

# RUN apt install git -y

RUN mkdir /root/.ssh/

ADD id_rsa /root/.ssh/id_rsa

RUN git clone -b $BRANCH $GIT_ADDRESS

WORKDIR $PROJECT_NAME

ENV HOST 0.0.0.0
ENV PORT 8080

RUN npm install

# RUN npm install koa koa-router -g

EXPOSE 8080

CMD npm run start
# CMD ["npm", "start"]
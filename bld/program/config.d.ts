interface Config {
    SERVER_PORT: number;
    branchNameStyle: string;
    projectPrefix: string;
}
export declare const config: Config;
export {};

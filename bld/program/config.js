"use strict";
exports.__esModule = true;
exports.config = {
    SERVER_PORT: 8080,
    branchNameStyle: '/',
    projectPrefix: 'makeflow-web-test'
};

"use strict";
exports.__esModule = true;
var Koa = require("koa");
var bodyParser = require("koa-bodyparser");
var config_1 = require("./config");
var proxy = require('redbird')({ port: 8088 });
var services_1 = require("./services");
var app = new Koa();
app.use(bodyParser.json());
var portService = new services_1.PortService();
var dockerService = new services_1.DockerService();
// const indexService = new IndexService(app, portService)
var routerService = new services_1.RouterService(portService, dockerService);
app.use(routerService.route());
function listen() {
    var port = config_1.config.SERVER_PORT || 8080;
    app.listen(port, '0.0.0.0', function () {
        console.info('this koa server is running on port ' + port);
    });
}
exports.listen = listen;

export interface dockerArgs {
    [key: string]: string;
}
export declare class DockerService {
    run(branch: string, port: number, args: dockerArgs): Promise<void>;
}

import { PortService } from "./port-service";
export declare class IndexService {
    private portService;
    constructor(portService: PortService);
    render(): string;
}

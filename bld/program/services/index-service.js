"use strict";
exports.__esModule = true;
var utils_1 = require("../utils");
var IndexService = /** @class */ (function () {
    function IndexService(portService) {
        this.portService = portService;
    }
    IndexService.prototype.render = function () {
        var branches = this.portService.branches.sort();
        return "\n            <html>\n                <head>\n                <title>MR Temporary Servers - Makeflow</title>\n                <style>\n                body {\n                    font-family: Helvetica, Verdana, sans-serif;\n                }\n\n                div {\n                    width: 720px;\n                    margin: 30px;\n                }\n\n                h2 {\n                    font-size: 38px;\n                    font-weight: 400;\n                    margin: 0 0 0 5px;\n                    padding: 0;\n                }\n\n                h4 {\n                    margin-left: 10px;\n                    font-size: 18px;\n                    font-weight: 200;\n                    color: #ddd;\n                }\n\n                ul {\n                    list-style-type: none;\n                    margin: 15px 0;\n                    padding: 0;\n                }\n\n                li {\n                    font-size: 18px;\n                    font-weight: 200;\n                    border-bottom: 1px solid #f6f6f6;\n                }\n\n                li:last-child {\n                    border: none;\n                }\n\n                li a {\n                    text-decoration: none;\n                    color: #999;\n                    padding: 12px 10px;\n\n                    transition: background-color 0.3s ease, color 0.3s ease, padding-left 0.3s ease;\n                    display: block;\n                }\n\n                li a:hover {\n                    color: #000;\n                    background: #f6f6f6;\n                    padding-left: 30px;\n                    position: relative;\n                }\n\n                li a::before {\n                    content: '>';\n                    position: absolute;\n                    margin-left: -18px;\n                    color: rgb(41, 109, 255);\n                    opacity: 0;\n                    transition: opacity 0.5s ease;\n                }\n\n                li a:hover::before {\n                    opacity: 1;\n                }\n\n                li .tag {\n                    color: #fff;\n                    margin-right: 5px;\n                    font-size: 15px;\n                    padding: 3px 5px;\n                    border-radius: 3px;\n                    background-color: rgb(153, 153, 153);\n                }\n\n                li .tag.tag-feature {\n                    background-color: rgb(129, 203, 95);\n                }\n\n                li .tag.tag-fix {\n                    background-color: rgb(255, 187, 41);\n                }\n                </style>\n                </head>\n                <body>\n                <div>\n                    <h2>Temporary Servers</h2>\n                    <ul>\n                    " + branches
            .map(function (branch) {
            var name;
            var _a = branch.split('/'), type = _a[0], names = _a.slice(1);
            if (!names.length) {
                name = type;
                type = undefined;
            }
            else {
                name = names.join('/');
            }
            return "<li><a target=\"_blank\" href=\"http://" + utils_1.getFullHostnameFromBranch(branch) + "\">" + (type ? "<span class=\"tag tag-" + type + "\">" + type + "</span>" : '') + name + "</a></li>";
        })
            .join('') + "\n                    </ul>\n                    " + (branches.length === 0 ? '<h4>(empty)</h4>' : '') + "\n                </div>\n                </body>\n            </html>\n\n        ";
    };
    return IndexService;
}());
exports.IndexService = IndexService;

export declare class PortService {
    private branchToPortMap;
    private portSet;
    readonly branches: string[];
    getPort(branch: string): number | undefined;
    generate(branch: string): Promise<number>;
}

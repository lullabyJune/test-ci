import * as Koa from 'koa';
import { PortService } from './port-service';
import { DockerService } from './docker-service';
export declare class RouterService {
    private portService;
    private dockerService;
    constructor(portService: PortService, dockerService: DockerService);
    private initialize;
    route(): Koa.Middleware<Koa.ParameterizedContext<{}, {}>, {}>;
}

import { Dict } from 'tslang';
export declare const MR_SERVICE_HOSTNAME = "mr.makeflow.test";
export declare function getSubdomainFromBranch(branch: string): string;
export declare function getFullHostnameFromBranch(branch: string): string;
export declare function runCommand(command: string, env?: Dict<string> | undefined): Promise<void>;

interface Config {
    SERVER_PORT: number,
    branchNameStyle: string,
    projectPrefix: string,
    projectName: string
}

export const config: Config = {
    SERVER_PORT: 8080,
    branchNameStyle: '/',
    projectPrefix: 'makeflow-web-test',
    projectName: 'test'
}
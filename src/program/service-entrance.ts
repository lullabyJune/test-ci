import * as Koa from 'koa'
import * as bodyParser from 'koa-bodyparser'
import {config} from './config'
const proxy = require('redbird')({port: 8088})

import {
    RouterService,
    DockerService,
    IndexService,
    PortService,
} from './services'

const app = new Koa()

app.use(bodyParser.json())

const portService = new PortService()
const dockerService = new DockerService()
// const indexService = new IndexService(app, portService)

const routerService = new RouterService(
    portService,
    dockerService,
    // indexService,
)

app.use(routerService.route())

export function listen():void {
    let port = config.SERVER_PORT || 8080

    app.listen(port, '0.0.0.0', () => {
        console.info('this koa server is running on port ' + port)
    })
}
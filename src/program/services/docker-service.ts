import { getSubdomainFromBranch, runCommand } from "../utils";
import { config } from "../config";

export interface dockerArgs {
    [key: string]: string
}

export class DockerService {
    async run(branch: string, port: number, args: dockerArgs): Promise<void> {
        let subdomain = getSubdomainFromBranch(branch);

        let projectName = `${config.projectPrefix}-${subdomain}`

        // let composeCommand = `docker-compose --project -name ${projectName} --file docker-compose-mr.yml`
        // let composeCommand = `docker-compose --project -name ${projectName} --file docker-compose-test.yml`
        let argsStr = ''

        Object.keys(args).forEach(key => {
            argsStr += ` --build-arg ${key}=${args[key]} `
        })

        let buildDockerCommand = `docker build -t ${projectName}${argsStr} ../../../`
        let runDockerCommand = `docker run -p ${port}:${config.SERVER_PORT} ${projectName}`

        console.info(`Starting image makeflow-web-test-${subdomain}`)

        // await runCommand(
        //     `${buildCommand} up --force-recrete --always-recreate-deps --renew-anon-volumes`,
        // )
        await runCommand(buildDockerCommand)
        await runCommand(runDockerCommand)
    }
}
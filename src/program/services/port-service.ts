import * as getPort from 'get-port'

export class PortService {
    private branchToPortMap = new Map<string, number>()
    private portSet = new Set<number>()

    get branches(): string[] {
        return Array.from(this.branchToPortMap.keys())
    }

    getPort(branch: string): number | undefined {
        return this.branchToPortMap.get(branch)
    }

    async generate(branch: string): Promise<number> {
        let port = this.getPort(branch)

        if (port) {
            return port
        }

        port = await getPort()

        this.branchToPortMap.set(branch, port)
        this.portSet.add(port)

        return port
    }
}
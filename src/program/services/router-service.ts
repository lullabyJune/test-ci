import * as Koa from 'koa'
import * as Router from 'koa-router'
import { PortService } from './port-service';
import { DockerService, dockerArgs } from './docker-service';
import { IndexService } from './index-service';
import { config } from '../config';

const router = new Router()
let count = 0,
    arr = ['feature/fuck', 'fix/world']

export class RouterService {
    constructor (
        private portService: PortService,
        private dockerService: DockerService,
    ) {
        this.initialize()
    }

    private initialize(): void {
        router.get('/', (ctx) => {
            ctx.body = new IndexService(this.portService).render()
        })

        router.get('/test', async (ctx) => {
            // ctx.body = ctx.request
            
        })

        router.post('/gitlab', async (ctx) => {
            let dataSource = ctx.request.body['object_attributes']

            console.log(dataSource['source_branch'])

            let branch = dataSource['source_branch']
            let port = await this.portService.generate(arr[count++])

            let args: dockerArgs = {
                GIT_ADDRESS: dataSource.source['git_ssh_url'],
                BRANCH: branch,
                PROJECT_NAME: config.projectName
            }

            await this.dockerService.run(branch, port, args).catch(console.error)

            console.info(`now page is running`)
        })
    }

    route(): Koa.Middleware<Koa.ParameterizedContext<{}, {}>, {}> {
        return router.routes()
    }
}
import * as ChildProcess from 'child_process'
import * as Path from 'path'

import {config} from './config'
import {Dict} from 'tslang'

export const MR_SERVICE_HOSTNAME = 'mr.makeflow.test'

const PROJECT_DIR = Path.join(__dirname, '../..')

export function getSubdomainFromBranch(branch: string): string {
    return config.branchNameStyle && branch.replace(config.branchNameStyle, '-')
}

export function getFullHostnameFromBranch(branch: string): string {
    return `${getSubdomainFromBranch(branch)}.${MR_SERVICE_HOSTNAME}`
}

export async function runCommand(
    command: string,
    env?: Dict<string> | undefined
) {
    console.info(`$ ${command}`)

    let commandProcess = await ChildProcess.exec(command, {cwd: PROJECT_DIR, env})

    commandProcess.stdout.pipe(process.stdout)
    commandProcess.stderr.pipe(process.stderr)
}